
Abstraction ?

- Abstraction consists of both abstract methods and non abstract methods


- we cannot create an object for the abstract class


- if we want to use the methods of the abstract class then we need to inherit the abstract class and write the body definition of abstract methods in the sub class and we can call the non abstract methods of the abstract class by the sub class object

- A class is a abstract class if add an abstract keyword before the class keyword


- similarly if a method is abstract if the method has no body and if we place abstract keyword before the return type




