import java.io.*;
import java.util.*;



abstract class AbstractMethod {
	public abstract void firstMethod(); // this is the abstract method with no body 

	public abstract void secondMethod(); // this is the abstract method with no body


	public static void thirdMethod() { // this is the not a abstract method because abstract keyword is not used
		System.out.println("this is the third method in the long run");
	}
}


public class AbstractExample extends AbstractMethod {
	
	public void firstMethod() { // overrides the abstract method in abstract class
		System.out.println("this is the first method in the long run");
	}

	public void secondMethod() { // overrides the abstract method in abstract class
		System.out.println("this is the second method in the long run");
	}

	public static void main(String args[]) { // Driver method
		AbstractExample ae = new AbstractExample();
		ae.firstMethod();
		ae.secondMethod();
		ae.thirdMethod();
	}

}